import networkx as nx
import random
import math
from copy import deepcopy


def random_nodes_on_field(node_number, field_size, sink_location):
    """ create random note with given field size and node numbers """
    node_set = []
    node_values = random.sample(range(field_size[0] * field_size[1]), node_number)
    for value in node_values:  # calculate node coordinate
        node = [0, 0]  # new empty node
        node[0], node[1] = value // field_size[0], value % field_size[0]
        node_set.append(node)
    node_set[0] = sink_location
    return node_set


def graph_from_nodes(node_set, transmission_range, source_number, uniform=True):
    """ create a graph from a node set"""
    graph = nx.Graph()
    [graph.add_node(i, pos=pos) for i, pos in enumerate(node_set, start=0)]  # add node
    for i in range(1, source_number):
        if not uniform:
            graph.nodes[i]['report_size'] = random.randint(1, 5)  # non-uniform report size
        else:
            graph.nodes[i]['report_size'] = 1  # uniform report size
    for i in range(source_number, len(node_set)):
        graph.nodes[i]['report_size'] = 0  # replay has report size = 0
    for i, node_1 in enumerate(node_set, start=0):
        for k, node_2 in enumerate(node_set, start=0):
            node_distance = math.sqrt(math.pow(node_1[0] - node_2[0], 2) + math.pow(node_1[1] - node_2[1], 2))
            if transmission_range >= node_distance > 0:
                graph.add_edge(i, k)  # add edge if transmission range > distance between 2 node
    return graph


def add_path_as_edge(graph, path):
    """ add all edge of a path to a graph """
    new_graph = graph
    for k in range(len(path) - 1):
        new_graph.add_edge(path[k], path[k+1])
    return new_graph


def compute_spt(graph):
    """ find shortest path tree of given graph with source node = 0 """
    shortest_path_tree = nx.Graph()
    [shortest_path_tree.add_node(i) for i in range(len(graph.nodes))]  # add node
    for i in range(1, len(graph.nodes)):
        shortest_path = nx.shortest_path(graph, 0, i, weight='weight')
        shortest_path_tree = add_path_as_edge(shortest_path_tree, shortest_path)  # add edge
    return shortest_path_tree


def compute_last_tree(graph, a):
    """ compute (a b) LAST tree """
    """ not so correct - because spt in this one is special! """
    spt = compute_spt(graph)  # create spt tree first
    for edge in spt.edges:
        spt.edges[edge[0], edge[1]]['weight'] = graph.edges[edge[0], edge[1]]['weight']
    mst = nx.minimum_spanning_tree(graph, 'weight')  # then mst tree
    for edge in mst.edges:
        mst.edges[edge[0], edge[1]]['weight'] = graph.edges[edge[0], edge[1]]['weight']
    last_tree = deepcopy(mst)
    for i in nx.dfs_preorder_nodes(mst, 0):  # last tree algorithm ...
        if i != 0:
            shortest_path_in_mst = nx.shortest_path_length(mst, 0, i, weight='weight')
            spt_length = spt.edges[0, i]['weight']
            if shortest_path_in_mst > a * spt_length:
                last_tree.add_edge(0, i, weight=spt_length)
    last_tree = compute_spt(last_tree)
    return last_tree


def compute_cost(tree, q, transmission_cost):
    """ calculate cost of graph/tree, root as 0, aggregation ratio g  """
    cost = 0
    for i in tree.nodes:
        if i != 0:
            child = nx.descendants(tree, i)
            report_size = tree.nodes[i]['report_size']
            for node in child:
                report_size += tree.nodes[node]['report_size']
            cost += math.ceil(report_size / q) * transmission_cost
    return cost


def data_aggregation_tree(graph, source_node):
    """ return dat tree from given graph"""
    complete_graph = nx.complete_graph(len(source_node))  # complete graph first

    for edge in complete_graph.edges:  # set the weight of G' to shortest path in G
        complete_graph.edges[edge[0], edge[1]]['weight'] = nx.shortest_path_length(graph, edge[0], edge[1])

    last_tree = compute_last_tree(complete_graph, 2)  # compute the (3,2)-LAST tree in G'

    graph_2 = nx.Graph()
    for edge in last_tree.edges:  # add edge to G" as shortest path in G
        shortest_path_in_g = nx.shortest_path(graph, edge[0], edge[1])
        graph_2 = add_path_as_edge(graph_2, shortest_path_in_g)

    data_aggregation_tree = nx.DiGraph()  # dat is actually spt spanning U in G
    for i in range(1, len(source_node)):
        shortest_path_in_g2 = nx.shortest_path(graph_2, 0, i)
        data_aggregation_tree = add_path_as_edge(data_aggregation_tree, shortest_path_in_g2)

    return data_aggregation_tree
