import networkx as nx
import utilities as ut

node_number = 100
source_number = 70
sink_location = [50, 50]
field_size = [100, 100]
transmission_range = 40
transmission_cost = 3

""" create random nodes and a graph from those nodes """
node_set = ut.random_nodes_on_field(node_number, field_size, sink_location)
source_node = node_set[:source_number]
relay_node = node_set[source_number:]
graph_1 = ut.graph_from_nodes(node_set, transmission_range, source_number, uniform=False)
# nx.draw(graph_1)

""" save the nodes attributes for later use """
pos = nx.get_node_attributes(graph_1, 'pos')
report_size = nx.get_node_attributes(graph_1, 'report_size')

""" data aggregation tree """
dat = ut.data_aggregation_tree(graph=graph_1,
                               source_node=source_node)

""" draw and result"""
nx.draw(dat, pos, with_labels=True)
nx.set_node_attributes(dat, report_size, 'report_size')
for q in range(1, 50):
    print(ut.compute_cost(dat, q, transmission_cost))

print('miss me with dat shit!')



